const express = require('express');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const body = require('body-parser');
const base = require('./src/routers/index');
const port = process.env.PORT || 8088;



// Envs config
dotenv.config({path: '.envs/.nodejs'});

// App config
app.use(body.json());
app.use(express.static(path.join(__dirname, 'public')));

// Routes
app.use('/api', base);

const run_server = async () => {
    try {
        const mongo_url = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@nodejstest-snr6j.mongodb.net/${process.env.MONGO_COLLECTION}`;
        await mongoose.connect(mongo_url, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
        app.listen(port, () => {
            console.log(`Server running in ${port} port`)
        });
    } catch (error) {
        console.log(`Server shutting down with error: ${error}`)
    }
};

run_server();