const { Schema, model } = require('mongoose');

const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    lat: {
        type: String,
        required: true
    },
    lan: {
        type: String,
        required: true
    }
});

module.exports = model('City', schema);